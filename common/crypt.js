var crypto = require('crypto');

exports.hashPassword = function(password, salt){
    var saltedPassword = password + salt;
    var hashedPassword = crypto.createHash('sha256').update(saltedPassword).digest('hex');
    return hashedPassword;
};

exports.cryptPassword = function(password){
    var salt = generateSalt(16);
    var saltedPassword = password + salt;
    var hashedPassword = crypto.createHash('sha256').update(saltedPassword).digest('hex');
    
    var returnObj = {
        "hashedPassword": hashedPassword,
        "salt": salt
    };
    
    return returnObj;
};

exports.generateSessionId = function(){
    return generateSalt(64);
};

function generateSalt(length){
    var salt = crypto.randomBytes(length).toString('base64');
    return salt;
}