// This file is for logging in the database

var logScheme = new mongoose.Schema({
    timestamp: Number,
    messages: [
        {
            timestamp: Number,
            message: String,
            level: { type: Number, index: true }
        }
    ]
}, { collection: 'system' });

var LogDb = mongoose.model('System', logScheme);

var logmessages = [];

exports.add = function(message, priority){
    var level = 0;
    switch(priority){
        case "debug": {
            level = 0;
            break;
        }
        case "log": {
            level = 1;
            break;
        }
        case "warn": {
            level = 2;
            break;
        }
        case "error": {
            level = 3;
            break;
        }
        case "exception": {
            level = 4;
            break;
        }
        case "security": {
            level = 5;
            break;
        }
    }
    
    
    logmessages.push({
        timestamp: moment().unix(),
        message: message,
        level: level
    });
    writeLogToDb();
    return;
};

function writeLogToDb(){
    if (logmessages.length >= config.dbLogSize){
        var thisDoc = new LogDb({
            timestamp: moment().unix(),
            messages: logmessages
        });

        thisDoc.save(function (err, res) {
            if (!err) {
                log.info("Successfully stored dblog");
                logmessages = [];
            }
            else {
                log.error(err);
            }
        });
    }
}