log.info('Trying to connect mongodb');
log.info(config.databaseConnection);
connect();

async function connect(){
    var connected = false;
    for (var i = 0; i <= config.databaseReconnectCounter; i++) {
        if(connected) break;
        await mongoose.connect(config.databaseConnection, { useNewUrlParser: true }, async function(err, res2) {
            if (err) {
                log.error(err.stack);
                log.info("Trying reconnect " + i + " of " + config.databaseReconnectCounter);
                await sleep(10000); // Sleep 10s
            }
            else {
                log.info('mongodb connected');
                connected = true;
            }
        });
    }
    module.exports = mongoose;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

