var nodemailer = require('nodemailer');

exports.generateMail = function(appointment ,lang = "de"){
    // Generate Link
    let confirmationLink = "";
    let deletionLink = "";
    let providerName = "";
    let name = appointment.details.name;
    let lfrom = appointment.from;
    let lto = appointment.to;

    // Email body
    var template;
    var text;
    var subject;
    
    switch(lang){
        case "de": { 
            template = require('../mailTemplates/de_DE/newAppointment');
            text = template.getBody(name, lfrom, lto, confirmationLink, deletionLink);
            subject = template.subject(providerName);
            break;
        }
        case "en": {


            break;
        }
        default: return null;
    }
};

exports.confirmProvider = function(providerName, mail, link, lang){
    // Email body
    var template;
    var text;
    var subject;

    switch (lang) {
        case "de": {
            template = require('../mailTemplates/de_DE/confirmProvider');
            text = template.getBody(link);
            subject = template.subject(providerName);
            break;
        }
        case "en": {


            break;
        }
        default: return null;
    }

    sendMail(mail, subject, text, function(err, info){

    });
};

exports.confirmUser = function(name, mail, link, lang){
    // Email body
    var template;
    var text;
    var subject;

    switch (lang) {
        case "de": {
            template = require('../mailTemplates/de_DE/confirmUser');
            text = template.getBody(name, link);
            subject = template.subject();
            break;
        }
        case "en": {


            break;
        }
        default: return null;
    }

    sendMail(mail, subject, text, function (err, info) {

    });
};

var sendMail = function(to, subject, body, callback){
    var mailOptions = {
        from: config.mailSettings.user,
        to: to,
        subject: subject,
        html: body
    };

    // Send text
    var transporter = nodemailer.createTransport({
        service: config.mailSettings.service,
        auth: {
            user: config.mailSettings.user,
            pass: config.mailSettings.pass
        }
    });


    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            log.error(error);
            callback(error, null);
        } else {
            log.info('Email sent: ' + info.response);
            callback(null, info.response);
        }
    });
};

exports.cancelAppointment = function(mail, lang, message){
    // Email body
    var template;
    var text;
    var subject;

    switch (lang) {
        case "de": {
            template = require('../mailTemplates/de_DE/appointmentCancelled');
            text = template.getBody(message);
            subject = template.subject();
            break;
        }
        case "en": {


            break;
        }
        default: return null;
    }

    sendMail(mail, subject, text, function (err, info) {

    });
};