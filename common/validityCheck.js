// Put in the REST Body and the expected fields and check if they exist
exports.checkValiditySecurity = function(body, fields, callback){
    checkSecurity(body, function(result){
        if(!result){
            log.warn("(401) Securitycheck failed for " + JSON.stringify(body));
            callback(false, 401);
            return;
        }
        else{
            if(checkFields(body, fields)){
                callback(true, 200);
                return;
            }
            else{
                log.warn("(400) Securitycheck failed for " + JSON.stringify(body));
                callback(false, 400);
                return;
            }
        }
        
    });
};

exports.checkValidity = function(body, fields){
    return (checkFields(body, fields));
};

exports.validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

// Checks if provided customerId and sessionId match
var checkSecurity = function(body, callback){
    if (config.noLoginNeeded){
        callback(true);
        return;
    }
    var db = require('../route/user/userDatabaseConnector');
    if(body.providerId && body.userId && body.sessionId){
        db.checkSessionId(body.providerId, body.userId, body.sessionId, function(result){
            callback(result);
        });
    }
    else {
        log.warn('No providerId, userId or sessionId provided in request');
        log.warn(body);
        callback(result = null);
    }
};

var checkFields = function(body, fields){
    if (!fields){
        return true;
    }
    if (fields.length > 0){
        for(var i = 0; i < fields.length; i++){
            if(!body.hasOwnProperty(fields[i])){
                return false;
            }
        }
        return true;
    }
    else {
        // Not an array but a single string provided
        if(!body['fields']){
            return false;
        }
        return true;
    }

    return false;
};