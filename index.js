module.exports = config = require ('./config.js');
module.exports = moment = require('moment');
require("moment/locale/de");
moment.locale('de');

var express = require('express');
var bodyParser = require('body-parser');
module.exports =  mongoose = require('mongoose');
mongoose.set('debug', true);

var fs = require('fs');

const logOpts = {
    logFilePath:'Backend.log',
    timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
};
module.exports = log = require('simple-node-logger').createSimpleLogger(logOpts);
log.setLevel('info');

module.exports = validity = require('./common/validityCheck');
log.error(">>> Index.js");

var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}


var app = express();
app.use(allowCrossDomain);
app.use(bodyParser.json());


require('./routes')(app);
module.exports = dbLog = require('./common/dbLog'); // Log in Database. This can only be initialized after routes, because only then we have a valid db connection
var port = process.env.PORT || 3001;
app.listen(port);
log.error('Listening on port: ' + port);
