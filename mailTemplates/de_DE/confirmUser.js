exports.subject = function () {
    return "Bestätige dein Benutzerkonto bei OnTime";
}

exports.getBody = function (name, confirmationLink) {
    var body = `
        <h1>
            Hallo,
        </h1>
        <p>
            Der Benutzer <b>` +name+ `</b> hat sich als Administrator für das Unternehmen registriert.
            Um dem Benutzer die Rechte zu erteilen, klicke einfach auf folgenden Link, oder kopiere ihn in die Adresszeile deines Browsers:
            <h3><a href="`+ confirmationLink + `">` + confirmationLink + `</a></h3>
            <br />
            Sollte der Benutzer nicht bekannt sein, bitte ignoriere diese Email klicke nicht auf den Link, da sonst die Person Zugriff auf alle
            Kalender erhält.
            <br />
            <br />
            Du hast Fragen zum Produkt oder deinem Abonnement? Schreibe uns eine Email an <a href="mailto:support@ontime.com">Support@OnTime.com</a>
            <br />
            <br />
            Dein OnTime-Team
            </p>
        <div>
            Diese Email automatisch von OnTime erstellt. Antworten werden nicht gelesen.
        </div>
    `;

    return body;
};
