
exports.subject = function(providerName){
    return "Dein Termin bei " + providerName;
}

exports.getBody = function(name, lfrom, lto, confirmationLink, deletionLink){
    var body = `
        <h1>
            Hallo `+ name +`!
        </h1>
        <p>
            Dein Termin wurde erfolgreich erstellt! Es fehlt uns aber noch eine Bestätigung. 
            Klicke einfach auf folgenden Link, oder kopieren ihn in die Adresszeile deines Browsers:
            <br />
            <h4>`+ moment(lfrom).format("dddd, DD. MMMM YYYY") + ` - ` + moment(lto).format("dddd, DD. MMMM YYYY")  +`</h4>
            <br />
            <h3><a href="`+ confirmationLink +`">`+ confirmationLink +`</a></h3>
            <br />
            <br />
            Falls du an dem Termin doch keine Zeit hast, kannst du ihn unter folgendem Link wieder stornieren:
            <br />
            <br />
            <a href="`+ deletionLink + `">` + deletionLink +`</a>
            <br />
            Dort hast du auch die Möglichkeit, zu einem anderen Zeitpunkt einen neuen Termin zu vereinbaren.
        </p>
        <div>
            Diese Email automatisch von OnTime erstellt. Antworten werden nicht gelesen.
        </div>
    `;

    return body;
};
