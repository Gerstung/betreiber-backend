exports.subject = function () {
    return "Dein Termin wurde abgesagt!";
}

exports.getBody = function (message) {
    var body = `
        <h1>
            Hallo,
        </h1>
        <p>
            Dein Termin wurde leider abgesagt. Es wurde folgende Nachricht für dich hinterlassen: 
            <br>
            ` + message + `
            <br> 
            Bei Fragen wende dich bitte direkt an den Anbieter. Vielen Dank!
        </p>
        <div>
            Diese einmalige Email wurde automatisch von OnTime erstellt. Antworten werden nicht gelesen.
        </div>
    `;

    return body;
};
