
exports.subject = function(providerName){
    return "Bestätige dein Unternehmen " + providerName + " bei OnTime";
}

exports.getBody = function(confirmationLink){
    var body = `
        <h1>
            Hallo,
        </h1>
        <p>
            Dein Unternehmen wurde erfolgreich bei OnTime registriert! Es fehlt uns aber noch eine Bestätigung. 
            Klicke einfach auf folgenden Link, oder kopieren ihn in die Adresszeile deines Browsers:
            <h3><a href="`+ confirmationLink +`">`+ confirmationLink +`</a></h3>
            <br />
            <br />
            <br />
            Wir bedanken uns, dass du dich für OnTime entschieden hast.
        </p>
        <div>
            Diese einmalige Email wurde automatisch von OnTime erstellt. Antworten werden nicht gelesen.
        </div>
    `;

    return body;
};
