//exports.databaseConnection = 'mongodb://atlaswriter:MongoDB_writer@ontime-shard-00-00-vhj7e.azure.mongodb.net:27017,ontime-shard-00-01-vhj7e.azure.mongodb.net:27017,ontime-shard-00-02-vhj7e.azure.mongodb.net:27017/OnTime?ssl=true&replicaSet=OnTime-shard-0&authSource=admin&retryWrites=true'; //'mongodb://127.0.0.1/OnTime';
exports.databaseConnection = 'mongodb://127.0.0.1/OnTime';
exports.databaseReconnectCounter = 5;

exports.failedLogins = 5; // Amount of failed login attempts on same Email until account is locked
exports.failedLoginWait = 2000; // MS to wait after failed login to hinder DDOS attacks / bruteforcing

exports.momentFormatLong = "LLL";  // 7. Dezember 2018 17:43
exports.momentFormatTimeOnly = "LT"; // 17:43

exports.mailSettings = {
    service: 'gmail',
    user: 'OnTimeService77@gmail.com',
    pass: 'OnTime#2019'
};

exports.subscriptionRights = {
    'addMultipleCalendars': 2,
    'getCalendars': 1
};

exports.autoLogOff = 10 * 60 * 1000;    // Automatic user logoff (deletion of session id) in ms

exports.debug = true;
exports.noLoginNeeded = true;
exports.dbLogSize = 10; // Number of messages to store in a single log document in DB

exports.confirmProviderLink = "http://127.0.0.1:8081/confirm?id=";

exports.confirmUserLink = "http://127.0.0.1:8081/confirmUser?id=";