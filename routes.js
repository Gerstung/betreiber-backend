module.exports = function(app){
    // Connect to MongoDB
    require('./common/dbConnect');

    // Load all modules
    var users = require('./route/user/users');
    var livecheck = require('./route/system/livecheck');
    var provider = require('./route/provider/provider');
    var calendar = require('./route/calendar/calendar');
    
    app.get('/alive', livecheck.alive);
    app.post('/users/register', users.register);
    app.post('/users/login', users.login);
    app.post('/users/logout', users.logout);

    app.post('/provider/register', provider.registerProvider);
    app.post('/provider/update', provider.updateProvider);
    app.post('/provider/confirm', provider.confirmProvider);
    app.post('/provider/addCalendar', provider.addCalendar);
    app.post('/provider/getCalendarItems', provider.getCalendarItems);
    app.post('/provider/getCalendars', provider.getCalendars);
    app.post('/provider/getPublicProviderInformation', provider.getPublicProviderInformation);
    app.post('/provider/createCalendarItem', provider.createCalendarItem);
    app.post('/provider/deleteCalendarItem', provider.deleteCalendarItem);
    app.post('/provider/updateCalendarItem', provider.updateCalendarItem)

    app.post('/calendar/createAppointment', calendar.createAppointment);
    app.post('/calendar/updateCalendarItem', calendar.updateCalendarItem);
    app.post('/calendar/getFreeSlots', calendar.getFreeSlots);
    app.post('/calendar/getFreeSlotsAll', calendar.getFreeSlotsAll);
    app.put('/calendar/cancelAppointment', calendar.cancelAppointment);
    app.put('/calendar/confirmAppointment', calendar.confirmAppointment);
}