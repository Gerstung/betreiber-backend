var billingSchema = new mongoose.Schema({
    "providerId": { type: String, index: true },
    "date": Number,
    "datePretty": String,
    "paid": Boolean,
    "paidDate": Number,
    "sent": Boolean,
    "sentDate": Number,
    "amount": Number,
    "service": String,
    "details": {
        "name": String,
        "taxId": String,
        "adress": {
            "street": String,
            "number": String,
            "postalcode": Number,
            "town": String,
            "country": String,
            "additional": String
        },
        "tel": String,
        "mail": String,
        "business": String
    }
    
}, { collection: 'billing' });

var BillingDb = mongoose.model('Billing', billingSchema);