var db = require('./userDatabaseConnector');

exports.register = function(req, res){
    log.info("Creating new user");

    var fieldsToCheck = ["mail", "password", "providerId", "firstName", "lastName"];

    if(validity.checkValidity(req.body, fieldsToCheck)){
        var mail = req.body.mail;
        var providerId = req.body.providerId;
        var firstName = req.body.firstName;
        var lastName = req.body.lastName;
        var password = req.body.password;
        
        if (!validity.validateEmail(req.body.mail)) {
            // Invalid mail provided
            res.status(400).send({ message: "Invalid Email format" });
            return;
        }

        var providerDb = require('../provider/provider');
        providerDb.providerExists(providerId, function (exists) {
            if (exists) {
                if (password.length < 8) {
                    log.info("User creation cancelled, password not long enough");
                    res.status(400).send({ "message": "Password not long enough" });
                    return 0;
                }

                if (mail.length < 3) {
                    log.info("User creation cancelled, mail not long enough");
                    res.status(400).send({ "message": "Mail not long enough or none provided" });
                    return 0;
                }

                var hasUpperCase = /[A-Z]/.test(password);
                var hasLowerCase = /[a-z]/.test(password);
                var hasNumbers = /\d/.test(password);
                var hasNonalphas = /\W/.test(password);
                if (hasUpperCase + hasLowerCase + hasNumbers + hasNonalphas < 3) {
                    log.info("User creation cancelled, password not complex enough");
                    res.status(400).send({ "message": "Password not complex enough" });
                    return 0;
                }

                // Password seems okay

                // Lazy load crypt library
                var crypt = require('../../common/crypt');
                var cryptObj = crypt.cryptPassword(password);
                var hashedPassword = cryptObj.hashedPassword;
                var salt = cryptObj.salt;

                // Create new database entry
                db.createUserDocument(mail, hashedPassword, salt, providerId, firstName, lastName, function (obj) {
                    // Callback function, Status is coming from db.createUserDocument()
                    log.info("User " + mail + " successfully created");
                    res.status(obj.status).send({ "message": obj.message });

                    providerDb.getMailById(providerId, function(err, mail){
                        if(err){
                            log.error("Could not retrieve provider mail for id: " + providerId);
                        }
                        else{
                            // TODO
                        }
                    });

                });
            }
            else {
                res.status(400).send({ message: "Provider does not exist, is not confirmed or no providerId supplied" });
            }
        });
    }
    else{
        res.status(400).send({message: "Missing fields"});
    }   
};

exports.login = function(req, res){
    log.info("REQUEST /users/login");
    
    if (!validity.validateEmail(req.body.mail)) {
        // Invalid mail provided
        res.status(400).send({ message: "Invalid Email format" });
        return;
    }

    var forceLogin = false;
    if(req.body.force){
        forceLogin = true;
    }
    db.logUserIn(req.body.mail, req.body.password, forceLogin, function(obj){
        switch(obj.status){
            case 200: res.status(obj.status).send({"message": obj.message, "sessionId": obj.sessionId, "userId": obj.userId, "providerId": obj.providerId}); 
                break;
            case 403: res.status(obj.status).send({"message": obj.message});
                break;
            default: res.status(obj.status).send({"message": obj.message});
        }
    });
};

exports.logout = function(req, res){
    log.info("REQUEST /users/logout");
    var fieldsToCheck = ["userId", "sessionId"];
    validity.checkValiditySecurity(req.body, fieldsToCheck, function(good, call){
        if(good){
            db.logUserOutById(req.body.userId, function(obj){
                res.status(obj.status).send(obj);
                return;
            });
        }
        else{
            res.status(500).send({ "message": "Could not log out" });
            return;
        }
    });
};