var userSchema = new mongoose.Schema({
    "providerId": {type: String, index: true},
    "mail": {type: String, index: true},
    "password": String,
    "salt": String,
    "sessionId": String,
    "name": String,
    "confirmed": Boolean,
    "confirmedTimestamp": Number,
    "creationTime": String,
    "loginTime": Number,
    "lastActionOnDb": Number,
    "failedAttempts": Number,
    "newPassword": Boolean,
    "locked": Boolean,
},  {collection: 'users'});

var UserDb = mongoose.model('User', userSchema);

exports.createUserDocument = function(mail, password, salt, providerId, firstName, lastName, callback){
    // Check if mail already exists in DB
    checkIfMailExists(mail, function(exists){
        // this is the callback
        if(exists){
            log.info("Mail already exists in DB, exiting");
            callback({
                "status": 403,
                "message": "Mail already exists in DB"
            })
            return;
        }
        else{
            var thisUser = new UserDb({
                "providerId": providerId,
                "mail": mail.toLowerCase(),
                "password": password,
                "salt": salt,
                "sessionId": "",
                "name": firstName + "|" + lastName,
                "confirmed": false,
                "confirmedTimestamp": null,
                "creationTime": moment().unix(),
                "loginTime": null,
                "failedAttempts": 0,
                "newPassword": false,
                "locked": false
            })
    
            thisUser.save(function(err, thisUser){
                if (err) return console.error(err);
                else{
                    log.info('Saved new user to DB');
                    callback({
                        "status": 201,
                        "message": "User successfully created"
                    })
                }
            })
        }
    });
}

exports.logUserIn = function(mail, password, forceLogin,callback){
    mail = mail.toLowerCase();
	findUserByMail(mail, function(obj){
        if (obj){
            var dbHashedPassword = obj.password;
            var dbSalt = obj.salt;

            // Lazy load crypt library
            var crypt = require('../../common/crypt');
            var hashedPassword = crypt.hashPassword(password, dbSalt);
            if(dbHashedPassword === hashedPassword){
                // Password seems fine, check if already logged in, if its not a forced login
                if(obj.sessionId !== "" && forceLogin === false){
                    callback({
                        "status": 403,
                        "message": "User already logged in"
                    })
                }
                else if(obj.changeUserPassword){
                    callback({
                        "status": 405,
                        "message": "Change password"
                    })
                }
                else if(obj.locked){
                    callback({
                        "status": 423,
                        "message": "You tried " + obj.failedAttempts + " times to log into this account. This account is locked"
                    })
                }
                else{
                    // Actually Log the user in the database
                    var sessionId = crypt.generateSessionId();
                    var update = {
                        "sessionId": sessionId,
                        "loginTime": moment().unix(),
                        "failedAttempts": 0
                    }
                    updateUserDocument(obj._id, update, function(){
                        callback({
                            "status": 200,
                            "message": "User logged in",
                            "sessionId": sessionId,
                            "userId": obj._id,
                            "providerId": obj.providerId
                        })
                    })
                }
            }
            else if (dbHashedPassword !== hashedPassword && obj.failedAttempts >= config.failedLogins){
                updateUserDocument(obj._id, {locked: true, failedAttempts: obj.failedAttempts +1}, function(){
                    callback({
                        "status": 423,
                        "message": "You tried " + obj.failedAttempts + " times to log into this account. This account is locked"
                    })
                })
            }
            else{
                updateUserDocument(obj._id, {failedAttempts: obj.failedAttempts + 1 }, function () {
                    setTimeout(function() {
                        callback({
                            "status": 403,
                            "message": "Wrong password or Email"
                        });
                    }, config.failedLoginWait);
                });
            }
        }
        else{
            setTimeout(function() {
                callback({
                    "status": 403,
                    "message": "There is no user with this Email"
                });
            }, config.failedLoginWait);
        }
    })
}

exports.changeUserPassword = function(mail, callback){

}

exports.logUserOutById = function(id, callback){
    logUserOutById(id, function(res){
        callback(res);
    })
}

exports.confirmUser = function(userId, callback) {
    var update = {
        confirmed: true,
        confirmedTimestamp: moment().unix()
    }
    updateUserDocument(id, update, function () { 
        callback();
    });
}

var logUserOutById = function(id, callback){
    var update = {
        "sessionId": "",
        lastActionOnDb: null
    };
    updateUserDocument(id, update, function () {
        callback({
            "status": 200,
            "message": "User logged out"
        });
    })
}

exports.checkSessionId = function(providerId, userId, sessionId, callback){
    // Session ID needs to be longer 3 characters, or else the "hacker" could just request with sessionId = "" when user is logged out
    if(sessionId.length < 3){
        callback(false);
        return;
    }
    isUserActive(userId, function(userActive){
        if(userActive){
            UserDb.findById(userId, function (err, doc) {
                if (!err && doc) {
                    if (!err && doc.sessionId === sessionId && doc.providerId === providerId) {
                        callback(true);
                        return;
                    }
                    else {
                        callback(false);
                        return;
                    }
                }
                else {
                    callback(false);
                    return;
                }
            });
        }
        else{
            callback(false);
        }
    })
}

var findUserByMail = function(mail, callback){
    UserDb.findOne({"mail": mail}, function(err, docs){
        if(docs && !err) callback(docs);
        else{
            log.error ("Error while retrieving user by mail from db");
            callback(null)
        }
    })
}

var updateUserDocument = function(id, update, callback){
    UserDb.findByIdAndUpdate(id, update, function(err, docs){
        if(err){
            log.error(err);
            callback(err);
        }
        else{
            callback();
        }
    })
}

var checkIfMailExists = function(mail, callback){
    UserDb.findOne({"mail": mail}, function(err, docs){
        if(docs){callback(true);}
        else {callback(false)}
    })
}

var isUserActive = function(id, callback){
    // Is called in every other function in this connector
    // If it returns false the user will log out automatically

    UserDb.findById(id, function(err, doc){
        if(!err){
            if(doc){
                var lastUserActionTime = doc.lastActionOnDb;
                if(lastUserActionTime === null){
                    // Fresh session, last action is empty so the user can continue
                    callback(true);
                    return;
                }
                else{
                    var actualTime = moment().unix();
                    if (actualTime - lastUserActionTime <= config.autoLogOff) {
                        callback(true);
                        return;
                    }
                    else{
                        callback(false);
                        // Log user out and reset the lastActionOnDb property
                        logUserOutById(id, function(res){});
                    }
                }
            }
            else{
                log.error("isUserActive: Cannot find user document by ID: " + id);
                callback(false);
            }
        }
        else{
            log.error("isUserActive: Following error occured:");
            log.error(err);
            callback(false);
        }
    })

}

var resetUserActiveTime = function(id){
    var update = {
        lastActionOnDb: moment().unix()
    }
    updateUserDocument(id, update, function(){});
}

