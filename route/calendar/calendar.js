var db = require('./calendarDatabaseConnector');

exports.createAppointment = function(req, res){
    log.info("REQUEST /calendar/createAppointment");
    
    var fieldsToCheck = [
        "calendarId",
        "from",
        "to",
        "details"
    ];
    // ValidityCheck
    if(validity.checkValidity(req.body, fieldsToCheck)){
        
        // Check if the details field is properly filled
        if(validity.checkValidity(req.body.details, ["name", "familyname", "mail"])){
            // Everything seems fine, processs
            db.createEntry("appointment", req.body.providerId, req.body.calendarId, req.body.from, req.body.to, req.body.details, false, function(result){
                switch(result.status){
                    case 201: res.status(201).send({"message": result.message}); return;
                    default: res.status(result.status).send(result); return;
                }
            });
        }
        else{
            res.status(400).send({
                "message": "Invalid Request, missing details",
                "cmessage": "1_15",
            });
            return;
        }
    }
    
    else{
        res.status(400).send({
            "message": "Invalid Request, missing fields",
            "cmessage": "1_15",
        });
        return;
    }    
};

exports.createAppointmentUnsafe = function (providerId, calendarId, blocker, from, to, name, details, callback){
    // This function is only called from provider, because there has to be no additional info besides from, to and name
    details["name"] = name;
    var type = "appointment";
    if(blocker) type = "blocker";
    db.createEntry(type, providerId, calendarId, from, to, details, true, function (result) {
        callback(result);
    });
};

exports.createAppointmentWithService = function(req, res){
    // Without Security
    res.status(500).send({message: "Not implemented"});
};

exports.getFreeSlots = function(req, res){
    // Finds free slots for a single calendarId
    log.info("REQUEST /calendar/getFreeSlots");
    
    let fieldsToCheck = [
        "calendarId",
        "from",
        "to",
    ];
    // ValidityCheck
    if (validity.checkValidity(req.body, fieldsToCheck)) {
        db.findFreeTimeslots(req.body.calendarId, req.body.from, req.body.to, req.body.minLength || 0, function(result){
            if(result){
                res.status(200).send({
                    "message": "Query successful", 
                    "calendarId": req.body.calendarId ,
                    "timeslots": result,
                    "cmessage": "1_16",
                });
                return;
            }
            else{
                res.status(404).send({
                    "message": "No free timeslot found.",
                    "cmessage": "1_17",
                });
                return;
            }
        })
    }
    else{
        res.status(401).send({ 
            "message": "Mandatory fields missing.",
            "cmessage": "1_18",
         });
        return;
    }
};

exports.getFreeSlotsAll = function(req, res){
    //Finds free slots for all calendards of one provider
    log.info("REQUEST /calendar/getFreeSlotsAll");
    
    let fieldsToCheck = [
        "providerId",
        "from",
        "to",
    ];

    if (validity.checkValidity(req.body, fieldsToCheck)) {
        db.findFreeTimeslotsAll(req.body.providerId, req.body.from, req.body.to, req.body.minLength || 0, function(result){
            switch(result.status){
                case 200:   res.status(200).send(result); break;
                case 404:   res.status(404).send(result); break;
                default:    res.status(500).send({"cmessage": "1_19"}); break;
            }
        });
    }
    else{
        res.status(401).send({ 
            "message": "Mandatory fields missing.",
            "cmessage": "1_18",
        });
        return;
    }
};

exports.cancelAppointment = function(req, res){
    log.info("REQUEST /calendar/cancelAppointment");

    let fieldsToCheck = [
        "appointmentId"
    ];

    if(validity.checkValidity(req.body, fieldsToCheck)){
        db.cancelAppointment(req.body.appointmentId, function(response){
            res.status(response.status).send(response);
        });
    }
};

exports.confirmAppointment = function (req, res) {
    log.info("REQUEST /calendar/confirmAppointment");

    let fieldsToCheck = [
        "appointmentId"
    ];

    if (validity.checkValidity(req.body, fieldsToCheck)) {
        db.confirmAppointment(req.body.appointmentId, function (response) {
            res.status(response.status).send(response);
        });
    }
};

exports.getCalendarItemsForId = function (calendarId, lfrom, lto, type, cancelled, confirmed, callback){
    db.getEntriesForId(calendarId, lfrom, lto, type, cancelled, confirmed, function(result){
        callback(result);
    });
};

exports.updateCalendarItem = function(req, res){
    // TODO

    res.status(500).send({message: "Not implemented"});
};

exports.deleteCalendarItem = function(calendarItemId, callback){
    db.deleteEntryForId(calendarItemId, function(res){
        if(res.status !== 200){
            callback(err, null);
        }
        else{
            callback(null, res);
        }
    });
};

exports.updateCalendarItemProvider = function(itemId, update, callback){
    db.updateCalendarItem(itemId, update, function(res){
        callback(res);
    });
};