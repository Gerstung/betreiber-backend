// Whitelisted fields for provider update
var fieldsToUpdate = ["from", "to", "type", "details"];

var calendarSchema = new mongoose.Schema({
    "providerId": {type: String, index: true},
    "calendarId": {type: String, index: true},
    "from": Number,
    "to": Number,
    "length": Number,
    "created": Number,
    "type": String, // "appointment / blocker / closing"
    "serviceId": String,    // ID of the specified service for that appointment
    "confirmed": Boolean,
    "confirmedTime": Number,
    "cancelled": Boolean,
    "cancelledTime": Number,
    "details":{
        "name": String,
        "familyname": String,
        "mail": String,
        "tel": String,
        "comments": String
    }
}, {collection: 'calendars'});

var CalendarDb = mongoose.model('Calendar', calendarSchema);


// Checks if the Timeslot of the favoured appointment is free
// Also checks if the given from and to time are not in the past
var checkTimeslotFree = function(lCalendarId, from, to, callback){
    
    // The next appointment starts before this one ends
    var fromQuery = {
        "$lt": to
    };

    // The appointment before is still ongoing before this one starts
    var toQuery = {
        "$gt": from
    };

    CalendarDb.findOne({ calendarId: lCalendarId, from: fromQuery, to: toQuery }, function (err, docs){
        if(!err){
            if(!docs){
                // No errors and I didn't find a document in this timerange
                callback(true);
                return;
            }
        }
        else{
            log.error(err);
        }

        callback(false);
    });
};

// Finds all free timeslots in the given timerange from the calendar
// Returns an array of objects with {from: unix, to: unix}
exports.findFreeTimeslots = function (lCalendarId, from, to, minLength, callback){
    findSlotsForCalendar(lCalendarId,from,to,minLength,function(res){
        callback(res);
    });
};

exports.findFreeTimeslotsAll = function(lproviderId, from, to, minLength, callback){
    // First find all Calendars for Provider
    // TODO: Implement parallel Appointments
    var providerDb = require('../provider/provider');
    providerDb.getCalendarsForProvider(lproviderId, function(calendars){
        if(calendars){
            var callbackCounter = calendars.length;
            var returnObj = [];

            calendars.forEach(function(calendar){
                let calendarId = calendar.calendarId;
                let calendarDetails = calendar;
                delete calendarDetails._id;
                findSlotsForCalendar(calendarId, from, to, minLength, function(slots){
                    log.info(slots);
                    callbackCounter--;
                    let newObj = {
                        "calendar": calendarDetails,
                        "slots": slots,
                    };
                    returnObj.push(newObj);
                    if(callbackCounter === 0){
                        callback({
                            status: 200,
                            calendars: returnObj,
                            cmessage: "1_1",
                            message: "Found calendars"
                        });
                    }
                });
            });
        }
        else{
            callback({
                status: 404,
                calendars: [],
                cmessage: "1_2",
                message: "There are no calendars for this provider"
            })
        }
    })
}

var findSlotsForCalendar = function(lCalendarId, from, to, minLength, callback){
    // The next appointment starts before this one ends
    log.info("Finding free slots for " + lCalendarId + " from " + from + " to " + to + " with min length " + minLength);
    var fromQuery = {
        "$lt": to
    }

    // The appointment before is still ongoing before this one starts
    var toQuery = {
        "$gt": from
    }
    
    CalendarDb.find({ calendarId: lCalendarId, from: fromQuery, to: toQuery, cancelled: false }, null, {sort: {from: 1}}, function (err, docs) {
        if (!err) {
            var freeSlots = [];
            if(docs.length > 0){ 
                log.info(docs);
                for (let i = 0; i < docs.length; i++) {
                    if(i === 0){
                        // First entry:
                        let fromLoop = from;
                        let toLoop = docs[i].from;
                        if(toLoop - fromLoop >= minLength){
                            freeSlots.push({ "from": fromLoop, "to": toLoop });
                        }
                    }
                    else{
                        // Middle entry:
                        let fromLoop = docs[i-1].to;
                        let toLoop = docs[i].from;
                        if (toLoop - fromLoop >= minLength) {
                            freeSlots.push({ "from": fromLoop, "to": toLoop });
                        }
                    }
                    
                }
                let fromLocal = docs[docs.length-1].to;
                let toLocal = to;
                if(toLocal - fromLocal > minLength){
                    freeSlots.push({ "from": fromLocal, "to": toLocal });
                }
            }

            else{

                callback([{
                    "from": from,
                    "to": to
                }]);
                return;
            }
            
            callback(freeSlots);
            return;
        }
        else {
            callback(null);
            return;
        }
    });
}

exports.createEntry = function(type, providerId, calendarId, from, to, details, unsafe, callback){
    var thisCalendar = new CalendarDb({
        "providerId": providerId,
        "calendarId": calendarId,
        "from": from,
        "to": to,
        "length": (to-from),
        "created": moment().unix(),
        "type": type, // "appointment / blocker / closing"
        "confirmed": false,
        "confirmedTime": 0,
        "cancelled": false,
        "cancelledTime": 0,
        "details": details
    })

    var providerDb = require('../provider/provider');
    providerDb.getCalendarsForId(calendarId, function(err, cal){
        if(err){
            log.error(err);
        }
        else{
            if(cal){
                if (cal.length !== (to - from)  && !config.debug){
                    // Appointment is not the same length as the calendar wants
                    callback({
                        "status": 401,
                        "cmessage": "1_3",
                        "message": "Length of appointment does not match calendar configuration"
                    });
                }
            }
        }
    });
    if(unsafe === false){
        checkTimeslotFree(calendarId, from, to, function (result) {
            if (result) {
                thisCalendar.save(function (err, thisCalendar) {
                    if (err) {
                        log.error(err);
                        callback({
                            status: 500,
                            message: err
                        });
                    }
                    else {
                        log.info('Saved new calendar item to DB');
                        callback({
                            "status": 201,
                            "cmessage": "1_4",
                            "message": "Calendar item successfully created"
                        })
                    }
                })
            }
            else {
                log.warn('Could not save calendar item, slot is used');
                callback({
                    "status": 401,
                    "cmessage": "1_5",
                    "message": "Timeslot is already occupied"
                });
            }
        });    
    }
    else{
        thisCalendar.save(function (err, thisCalendar) {
            if (err) {
                log.error(err);
                callback({
                    status: 500,
                    message: err
                });
            }
            else {
                log.info('Saved new calendar item to DB');
                callback({
                    "status": 201,
                    "cmessage": "1_4",
                    "message": "Calendar item successfully created"
                });
            }
        });
    }
        
};

exports.cancelAppointment = function (appointmentId, callback) {

    CalendarDb.findById(appointmentId, function (err, docs) {
        if (!err) {
            if (docs.cancelled !== true) {
                let update = {
                    "cancelled": true,
                    "cancelledTime": moment().unix(),
                }
                updateDocumentById(appointmentId, update, function (result) {
                    if (result.status !== 500) {
                        callback({
                            "status": 200,
                            "cmessage": "1_6",
                            "message": "Appointment cancelled"
                        });
                    }
                    else {
                        callback({
                            "status": 500,
                            "cmessage": "1_7",
                            "message": "Error while trying to cancel the appointment"
                        });
                    }
                });
            }
            else {
                callback({
                    "status": 200,
                    "cmessage": "1_8",
                    "message": "Appointment already cancelled"
                });
            }
        }
        else {
            callback({
                "status": 500,
                "cmessage": "1_7",
                "message": "Error while trying to cancel the appointment"
            });
        }
    })
};

exports.confirmAppointment = function(appointmentId, callback){
    
    CalendarDb.findById(appointmentId, function(err, docs){
        if(!err){
            if (docs.confirmed !== true){
                let update = {
                    "confirmed": true,
                    "confirmedTime": moment().unix(),
                }
                updateDocumentById(appointmentId, update, function(result){
                    if(result.status !== 500){
                        callback({
                            "status": 200,
                            "cmessage": "1_9",
                            "message": "Appointment confirmed",
                            "appointment": {
                                "from": moment(docs.from).format(config.momentFormatLong),
                                "to": moment(docs.to).format(config.momentFormatTimeOnly),
                                "details": docs.details   
                            }
                        });
                    }
                    else{
                        callback({
                            "status": 500,
                            "cmessage": "1_10",
                            "message": "Error while trying to confirm the appointment"
                        });
                    }
                });
            }
            else if (docs.cancelled === true){
                callback({
                    "status": 300,
                    "cmessage": "1_11",
                    "message": "Appointment already cancelled"
                });
            }
            else{
                callback({
                    "status": 300,
                    "cmessage": "1_12",
                    "message": "Appointment already confirmed"
                });
            }
        }
        else{
            callback({
                "status": 500,
                "cmessage": "1_10",
                "message": "Error while trying to confirm the appointment"
            });
        }
    })
};

var updateDocumentById = function(id, update, callback){
    CalendarDb.findByIdAndUpdate(id, update, function (err, doc) {
        if (!err) {
            // Times have changed, update length
            if(update.hasOwnProperty("from") && update.hasOwnProperty("to")){
                updateDocumentById(doc._id, { length: update.to - update.from}, function(res){
                    callback(res);
                });
            }
            else{
                callback({
                    "status": 200,
                    "cmessage": "1_13",
                    "message": "Update confirmed",
                    "doc": doc
                });
            }
        }
        else {
            log.warn("Error while updating");
            log.warn(err);
            callback({
                "status": 500,
                "message": "Error while trying to update the appointment"
            });
        }
    });
};

exports.getEntriesForId = function(calendarId, lfrom, lto, ltype, cancelled, confirmed, callback){
    var query = {
        calendarId: calendarId,
        from: { "$gte": lfrom },
        to: { "$lte": lto },
        type: ltype,
    };
    if(cancelled) query.cancelled = cancelled;
    if(confirmed) query.confirmed = confirmed;
    
    CalendarDb.find(query, null, { sort: { from: 1 } }, function(err, docs){
        if(!err){
            callback(docs);
        }
        else{
            callback(null);
        }
    });
};

exports.deleteEntryForId = function(appointmentId, callback){
    var update = {
        cancelled: true,
        cancelledTime: moment().unix()
    };
    updateDocumentById(appointmentId, update, function(res){
        callback(res);
    });
};

exports.updateCalendarItem = function(appointmentId, update, callback){
    var filterArray = [];
    for (var key in update) {filterArray.push(key)}
    var forbidden = filterArray.filter(value => fieldsToUpdate.includes(value));
    if(forbidden.length !== filterArray.length) {
        callback({
            status: 400,
            message: "Tried to change forbidden keys"
        });
        return;
    }
    else if(filterArray.includes("from") && !filterArray.includes("to")){
        callback({
            status: 400,
            message: "Tried to change times but no 'to' timestamp was provided"
        });
        return;
    }
    else if (!filterArray.includes("from") && filterArray.includes("to")){
        callback({
            status: 400,
            message: "Tried to change times but no 'from' timestamp was provided"
        });
        return;
    }

    else if (filterArray.includes("from") && filterArray.includes("to") && update.from >= update.to){
        callback({
            status: 400,
            message: "Tried to change times but 'from' time is bigger than 'to' time"
        });
        return;
    }

    else{
        updateDocumentById(appointmentId, update, function(res){
            callback(res);
        });
    }
    
};