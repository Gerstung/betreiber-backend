var db = require('./confirmationDatabaseConnector');

exports.createProviderConfirmationLink = function(providerId, providerMail, callback){
    var link = "";
    db.createConfirmDocument(providerId, providerMail, function(err, content){
        if(err){
            callback(err, null);
        }
        else{
            link = config.confirmProviderLink + content.id;
            callback(null, link);
        }
    })
}

exports.getProviderIdFromConfirmationId = function(confirmationId, callback){
    db.confirmDocument(confirmationId, function(err, id){
        if(err){
            callback(err, null);
        }
        else{
            callback(null, id);
        }
    })
}

exports.createUserConfirmationLink = function(userId, providerMail, callback){
    var link = "";
    db.createConfirmDocument(userId, providerMail, function (err, content) {
        if (err) {
            callback(err, null);
        }
        else {
            link = config.confirmUserLink + content.id;
            callback(null, link);
        }
    })
}

exports.getUserIdFromConfirmationId = function(confirmationId, callback){
    db.confirmDocument(confirmationId, function (err, id) {
        if (err) {
            callback(err, null);
        }
        else {
            callback(null, id);
        }
    })
}