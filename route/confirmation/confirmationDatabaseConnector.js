var confirmationSchema = new mongoose.Schema({
    "providerId": String,
    "mail": String,
    "created": Number,
    "confirmed": Number,
}, { collection: 'confirmation' });

var ConfirmDb = mongoose.model('Confirmation', confirmationSchema);

exports.createConfirmDocument = function (providerId, mail, callback) {
    var thisDoc = new ConfirmDb({
        "providerId": providerId,
        "mail": mail,
        "created": moment().unix(),
        "confirmed": null
    })

    thisDoc.save(function (err, doc) {
        if (err) {
            console.error(err); 
            callback(err, null);
        }
        else {
            log.info('Saved new confirmation to DB');
            callback(null, {
                "status": 201,
                "id": doc._id,
                "message": "Confirmation successfully created"
            })
        }
    })
}

exports.confirmDocument = function(confirmationId, callback){
    ConfirmDb.findById(confirmationId, function(err, doc){
        if(err){
            log.error(err);
            callback(err, null);
        }
        else {
            if(doc){
                confirm(confirmationId, function(err, res){
                    if(err){
                        log.warn("Could not update confirmation document but still got the provider Id");
                        callback(null, doc.providerId);
                    }
                    else{
                        callback(null, doc.providerId);
                    }
                })
            }
            else{
                callback(1, null);
            }
        }
    })
}

var confirm = function(confirmationId, callback){
    ConfirmDb.findByIdAndUpdate(confirmationId, {confirmed: moment().unix()} ,function(err, doc){
        if(err){
            log.error(err);
            callback(err, null);
        }
        else{
            if(doc == null){
                callback(1, null);
            }
            else{
                callback(null, doc)
            }
        }
    })
}
