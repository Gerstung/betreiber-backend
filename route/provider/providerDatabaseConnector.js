const FILENAME = "providerDatabaseConnector";

var providerSchema = new mongoose.Schema({
    "subscription": Number, // {0/null: none, the higher, the better}
    "name": String,
    "confirmed": Boolean,
    "confirmedTimestamp": Number,
    "details": {
        "owner": String,
        "adress": {
            "street": String,
            "number": String,
            "postalcode": Number,
            "town": String,
            "country": String,
            "additional": String
			// TODO: Umsatzsteuer ID
        },
        "tel": String,
        "mail": { type: String, index: true },
        "business": String,
        "slogan": String
    },
    "calendars": [
        {
            "calendarId": String,
            "calendarName": String,
            "calendarDescription": String,
            "parallelAppointments": Number,
            "start": Number,
            "end": Number
        }
    ]
},  {collection: 'providers'});


var ProviderDb = mongoose.model('Provider', providerSchema);

exports.createProvider = function(name, mail, optional, callback){
    providerExistsByMail(mail, function(exists){
        if(exists){
            callback({
                "status": 401,
                "message": "Provider already exists with this mail"
            });
        }
        else{
            if(!('details' in optional)) optional.details = {};
            if(!('adress' in optional.details)) optional.details.adress = {};

            var thisProvider = new ProviderDb({
                "subscription": optional.subscription || 0,
                "name": name,
                "confirmed": false,
                "confirmedTimestamp": null,
                "details": {
                    "owner": optional.details.owner || "",
                    "adress": {
                        "street": optional.details.adress.street || "",
                        "number": optional.details.adress.number || "",
                        "postalcode": optional.details.adress.postalcode || "",
                        "town": optional.details.adress.town || "",
                        "country": optional.details.adress.country || "",
                        "additional": optional.details.adress.additional || ""
                    },
                    "tel": optional.details.tel || "",
                    "mail": mail || "",
                    "business": optional.details.business || ""
                },
                "calendars": []
            });

            thisProvider.save(function (err, res) {
                if (!err) {
                    callback({
                        "status": 201,
                        "message": "New Provider saved",
                        "providerId": res._id
                    })
                }
                else {
                    callback({
                        "status": 500,
                        "message": "Error while saving the new Provider"
                    })
                    log.error(err);
                }
            })
        }
    })
}

exports.updateProviderSafe = function(id, update, callback){
    // TODO
    // Only Update fields that are empty. For overwriting use updateProviderUnsafe

    ProviderDb.findById(id, function(err, doc){
        if(!err){
            var existingProvider = doc;

            for(var key in update){
                if (update.hasOwnProperty(key)) {
                    if(key === "details"){
                        // iterate through the details object
                        for(var detailsKey in update.details){
                            if(update.details.hasOwnProperty(detailsKey)){

                                if (existingProvider.details.hasOwnProperty(key)) {
                                    if (existingProvider.details[key] == null || existingProvider.details[key] == "") {
                                        // Field in DB details object is empty, fill with content
                                        existingProvider.details[key] = update.details[key];
                                    }
                                }
                            }
                        }
                        // done with iterating of details object, continue with next key
                        continue;
                    }
                    else{
                        if (existingProvider.hasOwnProperty(key)) {
                            if (existingProvider[key] == null || existingProvider[key] == "") {
                                // Field in DB is empty, fill with content
                                existingProvider[key] = update[key];
                            }
                        }
                    }
                }
            }

            ProviderDb.findByIdAndUpdate(id, existingProvider, function(err,docs){
                if(!err){
                    callback({
                        "status": 200,
                        "message": "Successfully updated the provider"
                    })
                }
                else{
                    callback({
                        "status": 500,
                        "message": "Error while updating the provider " + id
                    })
                    log.error("Error while saving the update for provider: " + id);
                    log.error(err);
                }
            });

        }
        else{
            callback({
                "status": 500,
                "message": "Error while updating the provider " + id
            })
            log.error("Error while retrieving provider to update: " + id);
            log.error(err);
        }
    })

}

exports.updateProviderUnsafe = function (id, update, callback) {
    // TODO
}

exports.addCalendar = function(providerId, calendar, callback){
    ProviderDb.findById(providerId, function(err, doc){
        if(!err && doc){
            if (doc.subscription >= config.subscriptionRights.addMultipleCalendars){
                var index = doc.calendars.length || 0;
                var calendarId = providerId + '_' + index
                calendar.calendarId = calendarId;

                ProviderDb.updateOne(
                    { _id: providerId },
                    { $push: { calendars: calendar } },
                    function (err, res) {
                        if (!err) {
                            callback({
                                "status": 200,
                                "calendarId": calendarId,
                                "message": "Sucessfully added new calendar"
                            });
                            return
                        }
                        else {
                            callback({
                                "status": 500,
                                "message": "Error while trying to add new calendar"
                            });
                            log.error(err);
                            return;
                        }
                    }
                )
            }
            else{
                // Insufficient Subscription rights
                callback({
                    "status": 401,
                    "message": "Subscriber has insufficient subscription rights (" + doc.subscription + "/" + config.subscriptionRights.addMultipleCalendars +")"
                });
            }
        }
        else{
            callback({
                "status": 500,
                "message": "Error while trying to add new calendar"
            });
            log.error(err);
            return;
        }
    })
}

exports.updateCalendarLengths = function(providerId, calendarId, update, callback){

}

exports.updateSubscription = function(){

}

exports.providerExists = function(providerId, callback){
    ProviderDb.findById(providerId, function(err, doc){
        if(!err && doc){
            if (doc._id !== "" && doc.confirmed === true){
                callback(true);
            }
            else callback(false);
        }
        else{
            callback(false);
        }
    })
}

var providerExistsByMail = function(mail, callback){
    ProviderDb.find({"details.mail": mail}, function(err,doc){
        if (!err && doc) {
            if (doc.length > 0) {
                callback(true);
            }
            else callback(false);
        }
        else {
            callback(false);
        }
    })
}

var getCalendarsForId = exports.getCalendarsForId = function(providerId, callback){
    ProviderDb.findById(providerId, function(err, doc){
        if(!err && doc){
            if (doc._id !== "" && doc.confirmed === true && doc.subscription >= config.subscriptionRights.getCalendars){
                callback(doc.calendars);
            }
            else callback(null);
        }
        else{
            callback(null);
        }
    })
}

exports.getCalendarById = function(calendarId, callback){
    var providerId = calendarId.split('_')[0];
    
    ProviderDb.findById(providerId, function (err, doc) {
        if (!err && doc) {
            if (doc._id !== "" && doc.confirmed === true && doc.subscription >= config.subscriptionRights.getCalendars) {
                doc.calendars.forEach(function(cal){
                    if (cal.calendarId === calendarId){
                        callback(null, cal);
                        return;
                    }
                })
                callback(null, null);
            }
            else callback(null, null);
        }
        else {
            callback(1, null);
        }
    })
}

exports.getCalendarItemsForId = function (providerId, lfrom, lto, cancelled, confirmed, callback){
    // First get the actual calendars for the provider
    getCalendarsForId(providerId, function(calendars){
        if(calendars){
            var calendarDb = require('../calendar/calendar');
            var requestCounter = calendars.length;
            var returnObj = {};
            returnObj.calendars = [];
            

            calendars.forEach(function(cal){
                var id = cal.calendarId;
                var calName = cal.calendarName;
                var calDesc = cal. calendarDescription;
                calendarDb.getCalendarItemsForId(id, lfrom, lto, "appointment", cancelled, confirmed, function(items){
                    requestCounter--;
                    if(items){
                        var newObj = {
                            calendarId: id,
                            calendarName: calName,
                            calendarDescription: calDesc,
                            items: items
                        };

                        returnObj.calendars.push(newObj);
                    }
                    
                    if(requestCounter === 0){
                        callback(null, returnObj);
                        return;
                    }
                })
            })
        }
        else{
            callback(404, {message: "No calendars found for this provider"});
            return;
        }
    })
}
exports.confirmProvider = function(providerId, callback){
    ProviderDb.findByIdAndUpdate(providerId, {confirmed: true, confirmedTimestamp: moment().unix()}, function(err, doc){
        if(err){
            callback(err, null);
        }
        else{
            if(doc == null){
                callback(1, null);
            }
            else{
                callback(null, doc);
            }
        }
    })
}

exports.getPublicProviderInformation = function(providerId, callback){
    ProviderDb.findById(providerId, function(err, doc){
        if(!err){
            if(doc && doc.confirmed === true){
                var returnObj = doc;

                // delete hidden keys:
                var keysToDelete = [
                    "subscription",
                    "confirmed",
                    "confirmedTimestamp",
                ];

                keysToDelete.forEach(function(key){
                    delete returnObj[key];
                });

                callback(null, returnObj);

            }
            else{
                callback(1, {message: "No provider with this ID found"});
            }
        }
        else{
            log.error(FILENAME + ".getPublicProviderInformation: Error in receiving the obj: " + err);
            callback(err, null);
        }
    })
}

var findProviderByMail = function(mail, callback){
    // ToDo
}

