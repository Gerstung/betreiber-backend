var db = require('./providerDatabaseConnector');

exports.registerProvider = function(req, res){
    log.info("REQUEST /provider/register");
    var fieldsToCheck = ["name", "mail"];

    if(validity.checkValidity(req.body, fieldsToCheck)){
        if (!validity.validateEmail(req.body.mail)){
            res.status(400).send({message: "Invalid Email format"});
            return;
        }
        db.createProvider(req.body.name, req.body.mail, {}, function(callback){
            if(callback.status === 201){
                let mailService = require('../../common/mailService');
                let confirmationService = require('../confirmation/confirmation');
                confirmationService.createProviderConfirmationLink(callback.providerId, req.body.mail, function(err, link){
                    if(err){
                        log.error("Error while generating provider confirmation link: " + err);
                    }
                    else{
                        log.info("Confirmation link created: "+link);
                        if(!config.debug){
                            mailService.confirmProvider(req.body.name, req.body.mail, link, "de");
                        }
                    }
                })
            }
            res.status(callback.status).send(callback);
        })
    }
    else{
        res.status(401).send({message: "Name or Mail missing"});
        return;
    }
}

exports.updateProvider = function(req, res){
    // TODO
    // Update the provider. If no mail and password is sent, use updateProviderSafe function
    // that can only fill fields that are still empty

    // If mail and password are sent and matching this provider, use updateProviderUnsafe
    res.status(500).send({message: "Function not implemented yet!!"})
}

exports.confirmProvider = function(req, res){
    // TODO
    // Format: {id: int}
    let confirmationService = require('../confirmation/confirmation');
    confirmationService.getProviderIdFromConfirmationId(req.body.id, function(err, providerId){
        if(err){
            res.status(400).send({ message: "ID not found in database" })

        }
        else{
            db.confirmProvider(providerId, function(err, doc){
                if(err){
                    res.status(400).send({message: "Could not confirm provider"});
                }
                else{
                    res.status(200).send({message: "Provider succesfully confirmed"});
                }
            })
        }
    })
}

exports.addCalendar = function(req, res){
    log.info("REQUEST /provider/addCalendar");
    var fieldsToCheck = ["providerId", "calendar", "start", "end"];
    validity.checkValiditySecurity(req.body, fieldsToCheck, function(success, status){
        if(success === true){
            // Times are encoded like hhmm -> 13:20 = 1320
            var start = req.body.start;
            var end = req.body.end;
            if(start > end || start == end || start < 0 || end < 0 || start > 24 || end > 24)
            var calendar = {
                "calendarName": req.body.calendar.name || "",
                "calendarDescription": req.body.calendar.description || "",
                "parallelAppointments": req.body.calendar.parallelAppointments || 1,
                "start": start,
                "end": end
            };
            db.addCalendar(req.body.providerId, calendar, function(callback){
                res.status(callback.status).send(callback);
                return;
            });  
        }
        else{
            switch(status){
                case 400: res.status(400).send({message: "Missing ProviderId or Calendar"}); return;
                case 401: res.status(401).send({message: "Access denied. Check permissions"}); return;
                default: res.status(500).send(); return;
            }
        }
    });
}

exports.removeCalendar = function(req, res){
    // TODO
    res.status(500).send({message: "NOT IMPLEMENTED"});
}

exports.providerExists = function(providerId, callback){
    db.providerExists(providerId, function(result){
        callback(result);
    })
}

exports.getCalendarsForProvider = function(providerId, callback){
    db.getCalendarsForId(providerId, function(res){
        callback(res);
    })
}

exports.getCalendarItems = function(req, res){
    log.info("REQUEST /provider/addCalendar");
    var fieldsToCheck = ["from", "to"];
    validity.checkValiditySecurity(req.body, fieldsToCheck, function (success, status) {
        if (success === true) {
            var lfrom = req.body.from || 0;
            var lto = req.body.to || 1923208815; // Year 2030 in Unix
            var cancelled = req.body.cancelled || null;
            var showOnlyConfirmed = req.body.showOnlyConfirmed || null;
            
            db.getCalendarItemsForId(req.body.providerId, lfrom, lto, cancelled, showOnlyConfirmed, function(err, items){
                if(!err){
                    res.status(200).send(items);
                }
                else{
                    res.status(404).send(items);
                }
                
            })
        }
        else {
            switch (status) {
                case 400: res.status(400).send(); return;
                case 401: res.status(401).send({ message: "Access denied. Check permissions" }); return;
                default: res.status(500).send(); return;
            }
        }
    });
}

exports.getMailById = function(providerId, callback){
    // TODO
}

exports.getCalendarsForId = function(calendarId, callback){
    db.getCalendarById(calendarId, function(err, res){
        callback(err, res);
    });
}

exports.getPublicProviderInformation = function(req, res){
    log.info("REQUEST /provider/getPublicProviderInformation");
    var fieldsToCheck = ["providerId"];
    if(validity.checkValidity(req.body, fieldsToCheck)){
        db.getPublicProviderInformation(req.body.providerId, function(err, info){
            if(!err){
                res.status(200).send(info);
            }
            else{
                res.status(500).send({message: "Cannot receive information for this provider"});
            }
        });
    }
    else{
        res.status(401).send({message: "Provider ID missing"});
    }
}

exports.getCalendars = function(req, res){
    
    
    res.status(500).send({ message: "Not implemented" });
    return;
    
    db.getCalendarsForId(providerId, function (cals) {
        res.status(200).send(cals);
        return;
    })
}

exports.createCalendarItem = function(req, res){
    log.info("REQUEST /provider/createCalendarItem");
    var fieldsToCheck = ["calendarId", "from", "to", "name"];
    validity.checkValiditySecurity(req.body, fieldsToCheck, function(success, status){
        if(success === true){
            // TODO: Check if that calendarId even belongs to this provider
            var calendarId = req.body.calendarId;
            var providerId = calendarId.split('_')[0];
            var blocker = false;
            if (req.body.hasOwnProperty("blocker")) blocker = true;

            if(providerId === req.body.providerId){
                var calDb = require('../calendar/calendar');
                calDb.createAppointmentUnsafe(providerId, calendarId, blocker,req.body.from, req.body.to, req.body.name, req.body.details || {}, function(result){
                    console.log(result.status);
                    console.log(result);
                    res.status(result.status).send(result);
                })
            }
            else{
                res.status(400).send({ message: "This calendarId does not belong to this providerId" });
                return;
            }
        }
        else{
            switch (status) {
                case 400: res.status(400).send({ message: "Access denied. Check permissions" }); return;
                case 401: res.status(401).send({ message: "Access denied. Check permissions" }); return;
                default: res.status(500).send({ message: "Access denied. Check permissions" }); return;
            }
        }
    })
}

exports.deleteCalendarItem = function(req, res){
    log.info("REQUEST /provider/deleteCalendarItem");
    var fieldsToCheck = ["calendarItemId", "message"];
    validity.checkValiditySecurity(req.body, fieldsToCheck, function (success, status) {
        if (success === true) {
            // lazy load calendar api
            var calendarDb = require('../calendar/calendar');
            calendarDb.deleteCalendarItem(req.body.calendarItemId, function(err, doc){
                if(err){
                    res.status(500).send({message: "Error while deleting calendar item"});
                    return;
                }
                else{
                    if (!config.debug){
                        var mailService = require('../../common/mailService');
                        mailService.cancelAppointment(doc.details.mail, "de", req.body.message);
                    }
                    res.status(200).send({message: "Appointment successfully deleted and customer notified"});
                }
            })
        }
        else {
            switch (status) {
                case 400: res.status(400).send({ message: "Access denied. Check permissions"}); return;
                case 401: res.status(401).send({ message: "Access denied. Check permissions"}); return;
                default: res.status(500).send({ message: "Access denied. Check permissions"}); return;
            }
        }
    });
}

exports.updateCalendarItem = function (req, res) {
    log.info("REQUEST /provider/updateCalendarItem");
    var fieldsToCheck = ["calendarItemId", "update"];
    validity.checkValiditySecurity(req.body, fieldsToCheck, function (success, status) {
        if (success === true) {
            var calendarDb = require("../calendar/calendar");
            calendarDb.updateCalendarItemProvider(req.body.calendarItemId, req.body.update, function(obj){
                res.status(obj.status).send(obj);
                return;
            })
        }
        else {
            switch (status) {
                case 400: res.status(400).send({ message: "Access denied. Check permissions" }); return;
                case 401: res.status(401).send({ message: "Access denied. Check permissions" }); return;
                default: res.status(500).send({ message: "Access denied. Check permissions" }); return;
            }
        }
    });
}