exports.alive = function(req, res){
    dbLog.add("Received a heartbeat signal", "debug");
    log.info("REQUEST: /alive");
    var response = {
        "message": "I'm alive!", 
        "time": moment()
    };
    res.status(200).send(response);
};