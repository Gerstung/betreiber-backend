// TODO

var servicesSchema = new mongoose.Schema({
    "providerId": { type: String, index: true },
    "name": String,
    "description": String,
    "length": Number,
    "preparation": Number,
    "postprocessing": Number,
    "price": Number,
    "priceUnit": String, // Währungen in ISO Abkürzung 'eur', 'usd', 'gbp', 'chf', ...
    "active": Boolean,
}, { collection: 'services' });

var ServicesDb = mongoose.model('Services', servicesSchema);