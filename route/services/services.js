// TODO

exports.createService = function(req, res){
    // With Security
    res.status(500).send({message: "Not implemented!"});
};

exports.deleteService = function (req, res) {
    // With Security
    res.status(500).send({ message: "Not implemented!" });
};

exports.getServices = function (req, res) {
    // With Security
    res.status(500).send({ message: "Not implemented!" });
};

exports.updateService = function (req, res) {
    // With Security
    res.status(500).send({ message: "Not implemented!" });
};

exports.getServicesForProvider = function(providerId, callback){
    // Without Security
    callback(500, {message: "Not implemented!"});
}

